package com.boxinator.boxinatorbe.models.dtos;

import com.boxinator.boxinatorbe.models.domain.ShipmentStatusEnum;
import com.fasterxml.jackson.annotation.JsonGetter;

import java.time.LocalDate;
import java.util.List;

//DTO for Shipment class to encapsulate the information to be used in the controller
public class ShipmentDto {
    private Long id;
    private double cost;
    private double weight;
    private String boxColour;
    private List<ShipmentStatusDto> statusHistory;
    private LocalDate createdDate;
    private Long destinationId;
    private String receiverName;
    private AccountDto account;
    private ShipmentStatusEnum status;

    //Constructor
    public ShipmentDto(Long id, double cost, double weight, String boxColour, List<ShipmentStatusDto> statusHistory, LocalDate createdDate, Long destinationId, String receiverName, AccountDto account, ShipmentStatusEnum status) {
        this.id = id;
        this.cost = cost;
        this.weight = weight;
        this.boxColour = boxColour;
        this.statusHistory = statusHistory;
        this.createdDate = createdDate;
        this.destinationId = destinationId;
        this.receiverName = receiverName;
        this.account = account;
        this.status = status;
    }

    //Default constructor
    public ShipmentDto() {
    }

    //GETTERS AND SETTERS WITH @JsonGetter annotation to parse the variables into the JSON format
    @JsonGetter("id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @JsonGetter("cost")
    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    @JsonGetter("weight")
    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @JsonGetter("box_colour")
    public String getBoxColour() {
        return boxColour;
    }

    public void setBoxColour(String boxColour) {
        this.boxColour = boxColour;
    }

    @JsonGetter("destination_id")
    public Long getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(Long destinationId) {
        this.destinationId = destinationId;
    }

    @JsonGetter("status_history")
    public List<ShipmentStatusDto> getStatusHistory() {
        return statusHistory;
    }

    public void setStatusHistory(List<ShipmentStatusDto> statusHistory) {
        this.statusHistory = statusHistory;
    }

    @JsonGetter("account")
    public AccountDto getAccount() {
        return account;
    }

    public void setAccount(AccountDto account) {
        this.account = account;
    }

    @JsonGetter("issue_date")
    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    @JsonGetter("receiver_name")
    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    @JsonGetter("status")
    public ShipmentStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ShipmentStatusEnum status) {
        this.status = status;
    }

}
