package com.boxinator.boxinatorbe.models.dtos;

import com.boxinator.boxinatorbe.models.domain.AccountTypeEnum;
import com.fasterxml.jackson.annotation.JsonGetter;

import java.time.LocalDate;

//DTO for Account class to encapsulate the information to be used in the controller
public class AccountDto {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private LocalDate birthday;
    private Long countryId;
    private String zipCode;
    private String phoneNumber;
    private AccountTypeEnum accountType;

    //Constructor
    public AccountDto(Long id, String firstName, String lastName, String email, LocalDate birthday, Long countryId, String zipCode, String phoneNumber, AccountTypeEnum accountType) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.birthday = birthday;
        this.countryId = countryId;
        this.zipCode = zipCode;
        this.phoneNumber = phoneNumber;
        this.accountType = accountType;
    }

    //Default constructor
    public AccountDto() {
    }

    //GETTERS AND SETTERS WITH @JsonGetter annotation to parse the variables into the JSON format
    @JsonGetter("id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonGetter("first_name")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonGetter("last_name")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonGetter("email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonGetter("birthday")
    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    @JsonGetter("country_id")
    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    @JsonGetter("zip_code")
    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @JsonGetter("phone_number")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @JsonGetter("account_type")
    public AccountTypeEnum getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountTypeEnum accountType) {
        this.accountType = accountType;
    }
}
