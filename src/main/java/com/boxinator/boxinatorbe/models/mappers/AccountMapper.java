package com.boxinator.boxinatorbe.models.mappers;

import com.boxinator.boxinatorbe.models.domain.Account;
import com.boxinator.boxinatorbe.models.dtos.AccountDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

//Used to map/convert entity to dto and dto to entity
@Mapper(componentModel = "spring")
public interface AccountMapper {

    //convert Entity to a DTO
    @Mapping(source = "country.id", target = "countryId")
    AccountDto mapDto(Account account);

    //Convert DTO to an Entity
    @Mapping(source = "countryId", target = "country.id")
    Account mapModel(AccountDto account);

}
