package com.boxinator.boxinatorbe.models.domain;

import javax.persistence.*;
import java.util.List;

// Country entity
@Entity
@Table(name = "country")
public class Country {

    //Id auto-increments
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "country_name" )
    private String countryName;

    @Column(name = "multiplier")
    private double multiplier;

    //Mapping one country to many accounts
    @OneToMany(mappedBy = "country")
    private List<Account> accounts;

    //Constructor
    public Country(Long id) {
        this.id = id;

    }

    //Default constructor
    public Country() {
    }

    // GETTERS AND SETTERS

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public double getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(double multiplier) {
        this.multiplier = multiplier;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }
}
