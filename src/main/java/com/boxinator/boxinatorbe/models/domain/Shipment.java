package com.boxinator.boxinatorbe.models.domain;

import org.hibernate.annotations.JoinFormula;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

//Shipment Entity
@Entity
@Table(name = "shipment")
public class Shipment {

    //Id auto-increments
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "cost")
    private double cost;

    @Column(name = "weight")
    private double weight;

    @Column(name = "box_colour")
    private String boxColour;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "shipment_id")
    private List<ShipmentStatus> statusHistory;

    /**
     * Querying for the last status recorded every time this field is accessed.
     */
    @ManyToOne
    @JoinFormula("(SELECT s.id FROM shipment_status s WHERE s.shipment_id = id ORDER BY s.update_time DESC LIMIT 1)")
    private ShipmentStatus latestStatus;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Column(name = "receiver_name")
    private String receiverName;

    //Mapping many shipment to one country
    @ManyToOne
    @JoinColumn(name = "destination_id")
    private Country destination;

    //MApping many shipments to one account
    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;

    //Constructor
    public Shipment(double cost, double weight, String boxColour, List<ShipmentStatus> statusHistory, LocalDate createdDate, String receiverName, Country destination, Account account) {
        this.cost = cost;
        this.weight = weight;
        this.boxColour = boxColour;
        this.statusHistory = statusHistory;
        this.createdDate = createdDate;
        this.receiverName = receiverName;
        this.destination = destination;
        this.account = account;
    }

    //Default constructor
    public Shipment() {
    }

    //GETTERS AND SETTERS

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getBoxColour() {
        return boxColour;
    }

    public void setBoxColour(String boxColour) {
        this.boxColour = boxColour;
    }

    public List<ShipmentStatus> getStatusHistory() {
        return statusHistory;
    }

    public void setStatusHistory(List<ShipmentStatus> statusHistory) {
        this.statusHistory = statusHistory;
    }

    public void addStatusHistory(ShipmentStatusEnum status) {
        if (this.statusHistory == null) {
            this.statusHistory = new ArrayList<>();
        }
        this.statusHistory.add(new ShipmentStatus(status, LocalDateTime.now()));
    }

    public ShipmentStatus getLatestStatus() {
        return latestStatus;
    }

    public void setLatestStatus(ShipmentStatus latestStatus) {
        this.latestStatus = latestStatus;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public Country getDestination() {
        return destination;
    }

    public void setDestination(Country destination) {
        this.destination = destination;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }
}
