package com.boxinator.boxinatorbe.controllers;

import com.boxinator.boxinatorbe.exceptions.ResourceNotFoundException;
import com.boxinator.boxinatorbe.models.domain.Country;
import com.boxinator.boxinatorbe.models.dtos.CountryDto;
import com.boxinator.boxinatorbe.models.mappers.CountryMapper;
import com.boxinator.boxinatorbe.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins="*")

//Country controller class - FULL CRUD for country table
public class CountryController {

    //Injections
    @Autowired
    private CountryMapper mapper;

    @Autowired
    private CountryService service;


    /**
     * Retrieve the country multiplier information.
     * @return dtoList, resp
     */
    @GetMapping(value = "/settings/countries")
    public ResponseEntity<List<CountryDto>> getAllCountries() {
        List<CountryDto> dtoList = service.getAllCountries();

        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(dtoList, resp);
    }

    /**
     * Add a new country with all relevant information.
     * @param id
     * @param country
     * @return savedCountry
     */
    @PostMapping(value = "/settings/countries/{country_id}")
    public CountryDto addNewCountry(@PathVariable(value = "country_id") Long id, @RequestBody CountryDto country) {
        Country savedCountry = service.addNewCountry(id, country);
        return mapper.mapDto(savedCountry);
    }

    /**
     * Used to update a specific country.
     * @param id
     * @param country
     * @return country, status
     */
    @PutMapping("/settings/countries/{country_id}")
    public ResponseEntity<CountryDto> updateCountry(@PathVariable(value = "country_id") Long id, @RequestBody CountryDto country) throws ResourceNotFoundException {
        HttpStatus status = service.updateCountry(id, country);
        return new ResponseEntity<>(country, status);
    }

}
