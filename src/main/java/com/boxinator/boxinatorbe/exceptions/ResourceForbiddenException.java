package com.boxinator.boxinatorbe.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Custom exception handler for controllers and services
 */
@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class ResourceForbiddenException extends Exception {

    public ResourceForbiddenException(String message){
        super(message);
    }

}
