package com.boxinator.boxinatorbe.services;

import com.boxinator.boxinatorbe.exceptions.ResourceNotFoundException;
import com.boxinator.boxinatorbe.models.domain.Country;
import com.boxinator.boxinatorbe.models.dtos.CountryDto;
import com.boxinator.boxinatorbe.models.mappers.CountryMapper;
import com.boxinator.boxinatorbe.repositories.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CountryService {

    //injecting repository with autowired to automatically generate the constructor
    @Autowired
    private CountryRepository countryRepository;

    //injecting mapper to covert Entity to Dto and Dto to Entity
    @Autowired
    private CountryMapper mapper;

    //get all countries
    public List<CountryDto> getAllCountries() {
        //find all countries in the repository
        List<Country> countries = countryRepository.findAll();

        List<CountryDto> dtoList = new ArrayList<>();

        //Loop through every shipment of response and add to list of DTO
        for (Country country: countries) {
            dtoList.add(mapper.mapDto(country));
        }
        //return a country list
        return dtoList;
    }

    //add a new country
    public Country addNewCountry(Long id, CountryDto country) {
        //mapping countryDto in country model
        Country model = mapper.mapModel(country);

        //setting id
        model.setId(id);

        //saving a new country in the repository
        Country savedCountry = countryRepository.save(model);

        //return a new country
        return savedCountry;
    }

    //update country
    public HttpStatus updateCountry(Long id, CountryDto country) throws ResourceNotFoundException {
        HttpStatus status;

        //checking if country exists
        if(!id.equals(country.getId())) {
            throw new ResourceNotFoundException("Country not found.");
        }

        //mapping countryDto to countryModel
        Country model = mapper.mapModel(country);
        countryRepository.save(model);

        //return ok response
        status = HttpStatus.OK;
        return status;
    }
}
