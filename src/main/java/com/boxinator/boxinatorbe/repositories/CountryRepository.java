package com.boxinator.boxinatorbe.repositories;

import com.boxinator.boxinatorbe.models.domain.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//Repository for CountryService
@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
}
