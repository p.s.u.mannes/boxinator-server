package com.boxinator.boxinatorbe;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@Disabled
@SpringBootTest
class BoxinatorBeApplicationTests {

	@Test
	void contextLoads() {
	}

}
